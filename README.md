# Hashistacc AMI


## Quick start

To build the Nomad and Consul AMI:

1. `git clone` this repo to your computer.
1. Install [Packer](https://www.packer.io/).
1. Configure your AWS credentials using one of the [options supported by the AWS
   SDK](http://docs.aws.amazon.com/sdk-for-java/v1/developer-guide/credentials.html). Usually, the easiest option is to
   set the `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY` environment variables.
1. Update the `variables` section of the `config.json` Packer template to configure the AWS region and Nomad version
   you wish to use.
1. Run `packer build config.json`.

When the build finishes, it will output the IDs of the new AMIs. 

Source: https://github.com/hashicorp/terraform-aws-nomad/tree/master/examples/nomad-consul-ami