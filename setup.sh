#!/usr/bin/env bash
set -e


function installDependencies() {
	echo "Installing dependencies..."
	sudo apt-get -qq update &>/dev/null
	sudo apt-get -yqq install unzip jq &>/dev/null
	sudo sed -i '1 i\nameserver 127.0.0.1' /etc/resolv.conf

	echo "Installing Docker..."
	curl -fsSL https://get.docker.com -o get-docker.sh
	sudo sh get-docker.sh
}

function installConsul() {
	echo "Fetching Consul..."
	cd /tmp
	curl -sLo consul.zip https://releases.hashicorp.com/consul/${CONSUL_VERSION}/consul_${CONSUL_VERSION}_linux_amd64.zip

	echo "Installing Consul..."
	unzip consul.zip >/dev/null
	sudo chmod +x consul
	sudo mv consul /usr/local/bin/consul
}

function installNomad() {
	echo "Fetching Nomad..."
	cd /tmp
	curl -sLo nomad.zip https://releases.hashicorp.com/nomad/${NOMAD_VERSION}/nomad_${NOMAD_VERSION}_linux_amd64.zip

	echo "Installing Nomad..."
	unzip nomad.zip >/dev/null
	sudo chmod +x nomad
	sudo mv nomad /usr/local/bin/nomad

	echo "Installing Python requirements..."
	sudo apt-get install -yqq python3-pip &>/dev/null
	sudo pip3 install --no-cache-dir python-nomad boto3

	curl -L -o cni-plugins.tgz https://github.com/containernetworking/plugins/releases/download/v0.8.4/cni-plugins-linux-amd64-v0.8.4.tgz
	sudo mkdir -p /opt/cni/bin
	sudo tar -C /opt/cni/bin -xzf cni-plugins.tgz
	sudo tee /etc/sysctl.d/bridge.conf > /dev/null <<EOF
net.bridge.bridge-nf-call-arptables = 1
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF
}

function installVault() {
	echo "Fetching Vault..."
	cd /tmp
	curl -sLo vault.zip https://releases.hashicorp.com/vault/${VAULT_VERSION}/vault_${VAULT_VERSION}_linux_amd64.zip

	echo "Installing Vault..."
	unzip vault.zip >/dev/null
	sudo chmod +x vault
	sudo mv vault /usr/local/bin/vault
}

function installEnvoy() {
	sudo apt-get -yqq install curl apt-transport-https ca-certificates curl gnupg-agent software-properties-common
	curl -sL 'https://getenvoy.io/gpg' | sudo apt-key add -
	sudo add-apt-repository "deb [arch=amd64] https://dl.bintray.com/tetrate/getenvoy-deb $(lsb_release -cs) stable"
	sudo apt-get -qq update 
	sudo apt-get install -yqq getenvoy-envoy
}

function configConsul() {
  sudo tee /etc/systemd/system/consul.service > /dev/null <<"EOF"
[Unit]
Description=Consul Agent
Requires=network-online.target
After=network-online.target
Wants=vault.service
After=vault.service

[Service]
Restart=on-failure
Environment=CONSUL_ALLOW_PRIVILEGED_PORTS=true
ExecStart=/usr/local/bin/consul agent -config-dir="/etc/consul.d" -dns-port="53" -recursor="172.31.0.2"
ExecReload=/bin/kill -HUP $MAINPID
KillSignal=SIGTERM
User=root
Group=root

[Install]
WantedBy=multi-user.target
EOF
}

function configNomad() {
  sudo tee /etc/systemd/system/nomad.service > /dev/null <<"EOF"
[Unit]
Description=Nomad
Wants=network-online.target
After=network-online.target
Wants=consul.service
After=consul.service
Wants=vault.service
After=vault.service

[Service]
Environment="VAULT_TOKEN=$(cat /etc/vault.d/sink)"
ExecReload=/bin/kill -HUP $MAINPID
ExecStart=/usr/local/bin/nomad agent -config /etc/nomad.d
KillMode=process
KillSignal=SIGINT
LimitNOFILE=infinity
LimitNPROC=infinity
Restart=on-failure
RestartSec=2
StartLimitIntervalSec=10
StartLimitBurst=3
TasksMax=infinity

[Install]
WantedBy=multi-user.target
EOF
}

function installAutoShutdown() {
  echo "Setting up auto shutdown script"
  sudo tee /opt/autoshutdown.py > /dev/null <<"EOF"
#!/usr/bin/env python3

import collections
import datetime
import sched
import time
import subprocess
import nomad

client_nomad = nomad.Nomad(secure=True, cert=("/mnt/nomad/certs/agent.crt", "/mnt/nomad/certs/agent.key"))
counter = 0

def check_jobs(now):
    global counter
    jobs = client_nomad.jobs.get_jobs()
    nodes = client_nomad.nodes.get_nodes()
    job_count = 0
    ready_nodes = 0
    for j in jobs:
        if j["Type"] == "service":
            job_count += list(j["JobSummary"]["Summary"].values())[0]["Running"]
    if job_count == 0:
        counter += 1
    else:
      counter = 0

    for node in nodes:
      if node["Status"] == "ready":
         ready_nodes += 1

    if counter == 60 and ready_nodes > 2:
        print("No jobs running for a while... Shutting down")
        subprocess.call(["shutdown", "-h", "now"])
    enter_next(s, check_jobs)

def enter_next(s, function):
    now = datetime.datetime.utcnow()
    now_next = now.replace() + datetime.timedelta(seconds=5)
    s.enterabs(
        time=now_next.timestamp(),
        priority=1,
        action=function,
        argument=(now_next,),
    )

if __name__ == '__main__':
    s = sched.scheduler(lambda: datetime.datetime.utcnow().timestamp(), time.sleep)
    enter_next(s, check_jobs)
    check_jobs
    while True:
        s.run()
EOF

  sudo tee /etc/systemd/system/autoshutdown.service > /dev/null <<"EOF"
[Unit]
Description=Housekeeping
Wants=network-online.target
After=network-online.target

[Service]
ExecReload=/bin/kill -HUP $MAINPID
ExecStart=/opt/autoshutdown.py
KillMode=process
KillSignal=SIGINT
LimitNOFILE=infinity
LimitNPROC=infinity
Restart=on-failure
RestartSec=2
StartLimitIntervalSec=10
StartLimitBurst=3

[Install]
WantedBy=multi-user.target
EOF

  sudo chmod +x /opt/autoshutdown.py  
}

function main() {
    installDependencies
    installConsul
    installNomad
    installVault
    installEnvoy
    configConsul
    configNomad
    installAutoShutdown
}

main